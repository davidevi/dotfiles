
# TODO / HACK: For some reason can't assign to PS1 more than once and have it work

# if `parse_git_branch()` exists
if [ -n "$(type -t parse_git_branch)" ]; then
    # if `kube_ps1()` exists
    if [ -n "$(type -t kube_ps1)" ]; then
        # export PS1 with kube_ps1
        export PS1='$(kube_ps1)\n\[\033[38;5;5m\]\w\[$(tput sgr0)\]\033[38;5;6m$(parse_git_branch)$(tput sgr0)\n> '
    else
        # export PS1 without kube_ps1
        export PS1='\[\033[38;5;5m\]\w\[$(tput sgr0)\]\033[38;5;6m$(parse_git_branch)$(tput sgr0)\n> '
    fi
else 
    # if `kube_ps1()` exists
    if [ -n "$(type -t kube_ps1)" ]; then
        # export PS1 with kube_ps1
        export PS1='$(kube_ps1)\n\[\033[38;5;5m\]\w\[$(tput sgr0)\]\n> '
    else
        # export PS1 without kube_ps1
        export PS1='\[\033[38;5;5m\]\w\[$(tput sgr0)\]\n> '
    fi
fi

