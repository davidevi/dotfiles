
export PYENV_ROOT="$HOME/.pyenv"
export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init --path)"

function pr() {
    poetry run "$@"
}

function prp() {
    poetry run python -m "$@"
}

function pri() {
    poetry run invoke "$@"
}
