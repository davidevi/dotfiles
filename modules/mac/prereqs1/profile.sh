
# ARM Macs require the following to be set in order to use Homebrew
eval $(/opt/homebrew/bin/brew shellenv)

# Enable brew bash completion
if [ -f "$(brew --prefix)/etc/bash_completion" ]; then
    source "$(brew --prefix)/etc/bash_completion"
fi

# Disable opinionated auto-update when running brew commands
alias brew='HOMEBREW_NO_AUTO_UPDATE=1 /opt/homebrew/bin/brew'

# `ls` with colors
function ls {
    /bin/ls -G "$@"
}

export BASH_SILENCE_DEPRECATION_WARNING=1

export BASH_COMPLETION_COMPAT_DIR="/usr/local/etc/bash_completion.d"
[[ -r "/usr/local/etc/profile.d/bash_completion.sh" ]] && . "/usr/local/etc/profile.d/bash_completion.sh"

alias bashprofile="source ~/_dev/dotfiles/profile.sh"
