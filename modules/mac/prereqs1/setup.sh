#!/bin/bash -i

# Homebrew
if ! command -v brew &> /dev/null
then
    echo "Homebrew not found, installing..."
    /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
else
    echo "Homebrew already installed"
fi

eval $(/opt/homebrew/bin/brew shellenv)

# If current shell is not bash, change it to bash
if [ "$SHELL" != "/bin/bash" ]; then
    echo "Changing shell to bash..."
    chsh -s /bin/bash
else
    echo "Shell is already bash"
fi

# Remove occurrences of source $HOME/_dev/dotfiles/profile.sh from ~/.bash_profile and ~/.bashrc
# so that this script is idempotent
sed -i '' '/source $HOME\/_dev\/dotfiles\/profile.sh/d' ~/.bash_profile
sed -i '' '/source $HOME\/_dev\/dotfiles\/profile.sh/d' ~/.bashrc

echo "source $HOME/_dev/dotfiles/profile.sh" >> ~/.bash_profile
echo "source $HOME/_dev/dotfiles/profile.sh" >> ~/.bashrc

touch custom.sh
echo "source $HOME/_dev/dotfiles/custom.sh" >> ~/.bash_profile
echo "source $HOME/_dev/dotfiles/custom.sh" >> ~/.bashrc

