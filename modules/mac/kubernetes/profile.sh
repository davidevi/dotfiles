alias k="kubectl"
source <(kubectl completion bash)
complete -o default -F __start_kubectl k


alias spaces="kubectl get namespace"
alias pods="kubectl get pods"
alias svc="kubectl get svc"
alias ing="kubectl get ing"
alias certs="kubectl get certificate"

function cx {
  kubectx $@
}

function ns {
  kubens $@
}
function _ns() {
  COMPREPLY=();
  local word="${COMP_WORDS[COMP_CWORD]}";
  COMPREPLY=($(compgen -W "$(kubectl get ns -o jsonpath='{$.items[*].metadata.name}')" -- "$word"));
}
complete -F _ns ns

source $HOME/_dev/dotfiles/modules/mac/kubernetes/files/kube-ps1.sh
