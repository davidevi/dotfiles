# Bash Profile Generator and Automated Software Provisioning

```
curl https://gitlab.com/davidevi/dotfiles/-/archive/master/dotfiles-master.zip -o dotfiles.zip
unzip dotfiles.zip -d $HOME/_dev/
mv $HOME/_dev/dotfiles-master $HOME/_dev/dotfiles
cd $HOME/_dev/dotfiles
chmod +x cli
./cli install default
```
